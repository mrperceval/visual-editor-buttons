<?php
/* Plugin Name: Visual Editor Buttons */

namespace MrPerceval\VisualEditorButtons;

if (!is_admin()) {
	return;
}

add_action('admin_init', __NAMESPACE__ . '\\admin_init');

function admin_init() {
	if (current_user_can('edit_posts') && current_user_can('edit_pages')) {
		add_action('admin_enqueue_scripts', __NAMESPACE__ . '\\admin_enqueue_scripts');
		add_filter('mce_buttons', __NAMESPACE__ . '\\mce_buttons');
		add_filter('mce_external_plugins', __NAMESPACE__ . '\\mce_external_plugins');
	}
}

function mce_buttons($buttons) {
	array_push($buttons, 'toggle_layout_item_button', 'toggle_roundel_button');
	return $buttons;
}

function mce_external_plugins($plugin_array) {
	$plugin_array['visual_editor_buttons'] = plugins_url('/visual-editor-buttons.js', __FILE__) ;
	return $plugin_array;
}

function admin_enqueue_scripts() {
	wp_enqueue_style('custom_tinymce_plugin', plugins_url('/visual-editor-buttons.css', __FILE__));
}