(function() {
	/* Register the buttons */
	tinymce.create('tinymce.plugins.VisualEditorButtons', {
		init: function(ed, url) {

			var getAncestor = function (childNode) {
				var ancestorNodes = ed.dom.getParents(childNode, function(node) {
					return node.nodeName !== 'BODY' && node.nodeName !== 'HTML';
				});
				if (ancestorNodes.length) {
					return ancestorNodes[ancestorNodes.length - 1];
				}
				return false;
			}

			var toggleWrap = function (outerClass, innerClass) {
				var selectedNode = ed.selection.getNode();
				var selectedAncestors = getAncestor(selectedNode);
				var selectedHtml = '';

				if (selectedAncestors) {
					selectedHtml = selectedAncestors.outerHTML;
				} else {
					var startNode = ed.selection.getStart();
					var endNode = ed.selection.getEnd();
					var startNodeAncestor = getAncestor(startNode) || startNode;
					var endNodeAncestor = getAncestor(endNode) || endNode;

					var startHelperId = ed.dom.uniqueId();
					var startHelper = ed.dom.create('div', {id: startHelperId});
					var endHelperId = ed.dom.uniqueId();
					var endHelper = ed.dom.create('div', {id: endHelperId});

					startNodeAncestor.parentNode.insertBefore(startHelper, startNodeAncestor);
					endNodeAncestor.parentNode.insertBefore(endHelper, endNodeAncestor);

					ed.save();

					var sibling = startHelper.nextSibling;

					selectedAncestors = [];
					while (sibling) {
						if (sibling.id !== endHelperId) {
							selectedAncestors.push(sibling);
							selectedHtml += sibling.outerHTML;
							sibling = sibling.nextSibling;
						} else {
							selectedAncestors.push(sibling.nextSibling);
							selectedHtml += sibling.nextSibling.outerHTML;
							sibling = false;
						}
					}
					ed.dom.remove(startHelper);
					ed.dom.remove(endHelper);
				}

				var uid = ed.dom.uniqueId();
				var replaceNode;
				var layoutOpeningPattern = new RegExp('<div class="(.*\ )?' + outerClass + '(\ .*)?"><div class="(.*\ )?' + innerClass + '(\ .*)?">', 'g');
				var layoutClosingPattern = new RegExp('</div></div>$', 'g');
				if (selectedHtml.match(layoutOpeningPattern) && selectedHtml.match(layoutClosingPattern)) {
					selectedHtml = selectedHtml.replace(layoutOpeningPattern, '');
					selectedHtml = selectedHtml.replace(layoutClosingPattern, '');
					replaceNode = ed.dom.create('div', {id: uid}, selectedHtml);
				} else {
					replaceNode = ed.dom.create('div', {id: uid}, '<div class="' + outerClass + '"><div class="' + innerClass + '">' + selectedHtml  + '</div></div>');
				}

				if (selectedAncestors) {
					ed.dom.replace(replaceNode, selectedAncestors);
				} else {
					ed.execCommand('mceInsertContent', false, replaceNode.outerHTML);
					ed.save();
					// ed.execCommand('mceCleanup');
					var replaceNode = ed.dom.get(uid);
				}
				ed.selection.select(replaceNode);
				ed.dom.remove(replaceNode, true);
				ed.save();
				ed.isNotDirty = true;
				return;
			};

			ed.addButton('toggle_roundel_button', {
				title: 'Toggle Roundle',
				icon: 'icon dashicons-marker',
				cmd: 'toggle_roundel_button_cmd'
			});

			ed.addCommand('toggle_roundel_button_cmd', function() {
				return toggleWrap('roundle', 'roundle-inner');
			});

			ed.addButton('toggle_layout_item_button', {
				title: 'Toggle Layout Item',
				icon: 'icon dashicons-layout',
				cmd: 'toggle_layout_item_button_cmd'
			});

			ed.addCommand('toggle_layout_item_button_cmd', function() {
				return toggleWrap('layout-item', 'layout-item-inner');
			});
		},
		createControl: function(n, cm) {
			return null;
		},
	});
	/* Start the buttons */
	tinymce.PluginManager.add('visual_editor_buttons', tinymce.plugins.VisualEditorButtons);
})();